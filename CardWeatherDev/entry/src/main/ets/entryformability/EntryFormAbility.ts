import formInfo from '@ohos.app.form.formInfo';
import formBindingData from '@ohos.app.form.formBindingData';
import FormExtensionAbility from '@ohos.app.form.FormExtensionAbility';
import formProvider from '@ohos.app.form.formProvider';
import { WeatherRequest } from '../pages/WeatherRequest'
import { WeatherModel, RealtimeWeatherData, WeatherData } from '../pages/WeatherModel';
import { serviceLoveBean } from '../pages/serviceLoveBean'

export default class EntryFormAbility extends FormExtensionAbility {
  onAddForm(want) {
    // Called to return a FormBindingData object.
    let formData = {};
    return formBindingData.createFormBindingData(formData);
  }

  onCastToNormalForm(formId) {
    // Called when the form provider is notified that a temporary form is successfully
    // converted to a normal form.
  }

  onUpdateForm(formId) {
    // Called to notify the form provider to update a specified form.
  }

  onChangeFormVisibility(newStatus) {
    // Called when the form provider receives form events from the system.
  }

  onFormEvent(formId, message) {
    // Called when a specified message event defined by the form provider is triggered.
    var serviceLove: serviceLoveBean = JSON.parse(message.toString())
    console.info('==========:' + message.toString())
    console.info('==========:222' + serviceLove.content)
    new WeatherRequest().httpRequest((contents:string)=>{
      // 解析数据
      var weatherModel: WeatherModel = JSON.parse(contents.toString())
      if (weatherModel.error_code == 0) {
        let realtime : RealtimeWeatherData = weatherModel.result.realtime
        // 设置数据
        console.info(`FormAbility onEvent, formId = ${formId}, message: ${JSON.stringify(message)}`);
        let formData = {
          'city': weatherModel.result.city, // 和卡片布局中对应
          'temperature': realtime.temperature, // 和卡片布局中对应
          'info': realtime.info, // 和卡片布局中对应
          'direct': realtime.direct, // 和卡片布局中对应
        };
        let formInfo = formBindingData.createFormBindingData(formData)
        formProvider.updateForm(formId, formInfo).then((data) => {
          console.info('FormAbility updateForm success.' + JSON.stringify(data));
        }).catch((error) => {
          console.error('FormAbility updateForm failed: ' + JSON.stringify(error));
        })
      } else {
        // 接口异常，弹出提示
      }
    })
  }

  onRemoveForm(formId) {
    // Called to notify the form provider that a specified form has been destroyed.
  }

  onAcquireFormState(want) {
    // Called to return a {@link FormState} object.
    return formInfo.FormState.READY;
  }
};