export class WeatherModel {
  reason: string //返回说明
  error_code: number //返回码，0为查询成功
  result: {
    city: string //城市信息
    realtime: RealtimeWeatherData // 实时天气
    future: Array<WeatherData> // 往后天气情况
  }
}

export class RealtimeWeatherData {
  info: string = '' // 天气情况
  temperature: string = '' // 当前温度
  direct: string = '' //风向
}

export class WeatherData {
  date: string // 日期
  temperature: string // 温度
  direct: string // 风向
  weather: string // 天气情况
}