import http from '@ohos.net.http';

export class WeatherRequest{
  constructor() {
  }

  // 接口实现
  async httpRequest(callBack:(content:string)=>void){
    let httpRequest = http.createHttp();
    httpRequest.on('headersReceive', (header) => {
      console.info('header: ' + JSON.stringify(header));
    });

    httpRequest.request(
      "http://apis.juhe.cn/simpleWeather/query?key=397c9db4cb0621ad0313123dab416668&city=西安",
      {
        method: http.RequestMethod.POST, // 可选，默认为http.RequestMethod.GET
        header: [{
          'Content-Type': 'application/json'
        }],
        // 当使用POST请求时此字段用于传递内容  post请求 http://apis.juhe.cn/simpleWeather/query
        extraData: {
          'key': '',
          'city': ''
        },
        expectDataType: http.HttpDataType.STRING, // 可选，指定返回数据的类型
        usingCache: true, // 可选，默认为true
        priority: 1, // 可选，默认为1
        connectTimeout: 60000, // 可选，默认为60000ms
        readTimeout: 60000, // 可选，默认为60000ms
        usingProtocol: http.HttpProtocol.HTTP1_1, // 可选，协议类型默认值由系统自动指定
      }, (err, data: http.HttpResponse) => {
      if (!err) {
        console.info('=====data.result=====' + data.result)
        callBack(data.result.toString())
        // 当该请求使用完毕时，调用destroy方法主动销毁
        httpRequest.destroy();
      } else {
        console.error('error:' + JSON.stringify(err));
        // 取消订阅HTTP响应头事件
        httpRequest.off('headersReceive');
        // 当该请求使用完毕时，调用destroy方法主动销毁
        httpRequest.destroy();
      }
     }
    );
  }
}

