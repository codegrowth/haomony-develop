export class ClassBean {
  private name: string
  private age: number

  constructor(name: string, age: number) {
    this.name = name;
    this.age = age;
  }

  public getInfo(): string {
    return `My name is ${this.name} and age is ${this.age}`;
  }
}