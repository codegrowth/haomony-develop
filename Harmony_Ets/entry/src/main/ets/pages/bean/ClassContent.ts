import { ClassBean } from './ClassBean';
export class ClassContent extends ClassBean {
  private sex: number

  constructor(name: string, age: number, sex: number) {
    super(name, age);
    this.sex = sex;
  }

  public getSexInfo(): string {
    return this.getInfo() + ` and work in ${this.sex}`;
  }
}