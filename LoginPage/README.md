



# 哪些人适合鸿蒙开发？

### 1.有移动端开发经验

OpenHarmony开发采用的是ArkTS语言，ArkTS是在TypeScript基础上扩展，而TypeScript又是JavaScript的超集，
如果之前有过JavaScript相关的前端开发经验 ，比如Vue，React Native等 学习OpenHarmony就会相对轻松
，可以迅速开发一个能在OpenHarmony运行的应用。

### 2.无移动端开发经验

如果没有JavaScript开发经验，但是有其他编程语言经验 比如：Java c/c++ Kotlin Swift ,
学习OpenHarmony也不会困难，可以补充学习JavaScript基础的知识来学习
OpenHarmony，因为JavaScript和ArkTS都是很直观容易学习的语言。

### 3.对OpenHarmony开发感兴趣

OpenHarmony也适合对它感兴趣的非开发人员，因为JavaScript和ArkTS都是很直观容易学习的语言，
比其他语言更简单入门更快，可以快速了解 OpenHarmony的特性和引用场景
