import http from '@ohos.net.http';
import prompt from '@ohos.prompt';

export class NetworkUtil{
  constructor() {
  }

  async getHomeData(pageNo: number, tabType: string,
      callBack:(content: string) => void) {
    // 创建http
    let httpRequest = http.createHttp()
    // 请求数据
    httpRequest.request('http://v.juhe.cn/toutiao/index',
      {
        // 看源码得知method的类型为：RequestMethod
        // 但是设置 method: http.RequestMethod.POST 报错
        // 设置成 method: http.POST 可以
        method: http.RequestMethod.POST,
        extraData: {
          'key': '9099932a8e3acd3dbdd4bd11fcc98738',
          'page_size': '10',
          'page': '' + pageNo,
          'type': '' + tabType,
        }
      },
      (err, data) => {
        if (!err) {
          if (data.responseCode == 200) {
            // 解析数据
            callBack(data.result.toString())
          } else {
            // 请求失败，弹出提示
            prompt.showToast({ message: '网络异常' })
          }
        } else {
          // 请求失败，弹出提示
          prompt.showToast({ message: err.message })
        }
      })
  }
}