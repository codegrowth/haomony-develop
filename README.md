# 大家好。

开门见山地说，如果你正在为学习鸿蒙开发而不知道该怎么学习，或者你学习鸿蒙老是遇到问题不知道怎么办，那么可以直接加入闪客的鸿蒙技术星球来，无脑冲就对了！

性价比高的离谱！还支持一对一服务。

![输入图片说明](ArrayDev/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20240406100042.png)

如果你之前还不了解鸿蒙开发，或者更不了解技术，第一次来，那么请花 10 分钟时间认认真真看完这篇内容，你会得到一个满意的答卷并选择立马加入。

先来做个介绍吧，闪客的这个鸿蒙技术课程是一个有关鸿蒙应用开发的编程学习课程 + 项目实战 ，学习完后还提供简历的优化和工作就业的指导。

你可以获取一个入门到实战的鸿蒙开发可成，及时无任何基础也会教会你怎么做开发、不限次数地提问、和闪客以及加入的小伙伴一起成长，除此之外，还会帮你制定学习计划、修改简历，你甚至可以一对一教你怎么做开发。

也是得到了很多老朋友的认可，所以才敢如此“信誓旦旦”地推荐给大家。

![输入图片说明](ArrayDev/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20240406100147.png)

目前只有这个课程是从“什么是HarmonyOS？”开始，带您领略与HarmonyOS之间的精妙关联，进而深入浅出地解释为何HarmonyOS是未来智能应用开发的理想平台。

通过接下来的模块，您将亲手进行开发工具和SDK的安装、配置，并且迎来编程世界的新语言——ArkTS。

我们不仅引导您完成第一个ArkTS应用的创建，还带您深入了解项目工程的架构与逻辑。

每一个章节都是精心设计，从页面布局到自定义组件生命周期的研究，从ArkUI组件的开发到基础和容器组件的实战应用，每一步都围绕着提升您的开发技能而展开。

刷新机制、界面的活性化，登录页面的研发，到头条首页的专项开发项目，直到商城项目和运动健康项目的实战，您将通过实战学习如何打磨用户体验。

此外，数据库和网络连接开发模块将成为您解锁数据灵活运用的关键。

而一旦您所需的天气信息轻松跃跃于网络连接之间，那么打包您的智能应用(Hap)、构建及利用library库，这一切的一切，都将指引您走向应用开发的高峰。

随着人数越来越多，质量和价格也会逐步提高，所以你目前看到的就是最低价，越往后去，价格只会越高，这是一定的。


### 一 课程预览

#### 课程包括15个大章，80多个小章，针对各个功能进行了详细讲解。如下：

![输入图片说明](ArrayDev/image.png)

![输入图片说明](ArrayDev/image3.png)

![输入图片说明](ArrayDev/image4.png)

还有一些实战项目，可以直接拿来学习使用，工作中也能作为一个项目经验来使用。

#### 星球有多个实战项目：
![输入图片说明](ArrayDev/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20240406101049.jpg)

#### 商城项目：

#### 项目源码讲解都在星球中，加入闪客的星球后可以看项目的拆分专栏，一步一步分析每一个功能的开发，支持一对一指导。

![输入图片说明](ArrayDev/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20240406101049.jpg)


![输入图片说明](ArrayDev/image5.png)

![输入图片说明](ArrayDev/image6.png)


### 二、还提供什么服务？
除了上面提到的高质量实操课程，还可以给你提供以下服务：

1）赠送 干货项目实战课程。后续该课程会制作成视频课程，会一起提供，并且后续的所有直播实战项目都会提供，并且还有一个项目实战课程开发中，一起赠送。

2）为你提供专属的一对一提问交流，如何准备面试，如何制定学习计划，如何选择 offer，都能得到我 1v1 的指导和建议；


### 三、下定决心加入了！
相信看到这里的你已经迫不及待、下定决心加入鸿蒙了，扫下面的二维码识别后加入。

![输入图片说明](ArrayDev/image8.png)

随着时间推移，积累的干货会越来越多，加入的小伙伴也会越来越多，课程内容和分类也会越来越多。所以价格也会逐步提高门槛。

加入记得添加我的微信（扫下面的二维码），备注「鸿蒙课程」我会拉你进 交流群，目前已经是第 2 群了。

![输入图片说明](ArrayDev/image8.png)

郑重声明：如果感觉自己没有技术底子，怕自己学不会 可以提供必要的帮助。

![输入图片说明](ArrayDev/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20240403214649.jpg)


共勉 ⛽️。


#### 开源知识---介绍

#### 技能
- [01、组件](#组件)

#### 资源下载

- [01、入门](#入门)
- [02、工具](#工具)
- [03、自定义](#自定义)
- [04、框架项目](#框架项目)
- [05、绘制](#绘制)


#### 安装教程

* [鸿蒙开发入门篇](https://mp.weixin.qq.com/s/l9y8DnICrwiXQlR1shC3LA)


#### 组件



#### 核心知识点

- 鸿蒙元服务资料       [鸿蒙元服务资料](https://gitee.com/codegrowth/haomony-develop/tree/master/DocData/DocData/%E5%85%83%E6%9C%8D%E5%8A%A1%E6%96%87%E6%A1%A3%E8%B5%84%E6%96%99)


#### 入门

- 《抓住机会，教你入门鸿蒙开发》        [抓住机会，教你入门鸿蒙开发](https://mp.weixin.qq.com/s/WQlkiNK-LbAy3hBVxbzhsA)


- 《HarmonyOS入门宝典2.0》        [百度云下载链接](https://pan.baidu.com/s/1XeYt_w1XPAZk4LUmH1NloA)  密码:hqsw
- 《HarmonyOS设备开发入门1.2版》       [百度云下载链接](https://pan.baidu.com/s/1qyQdlz4MpzS7YzW1KT7Vpw) 提取码:qroe
- 《华为鸿蒙HarmonyOS开发者资料汇总》       [百度云下载链接](https://pan.baidu.com/s/1xWrEIRGmfyjb6fjyAUgEtQ) 提取码:gj62
- 《HiSpark_WiFi_IoT智能小车开发套件安装教程（HarmonyOS/鸿蒙）》       [百度云下载链接](https://pan.baidu.com/s/1XkRNYZcrt7pjxXmLi5waJQ) 提取码:tk6u
- 《Hi3861V100软件开发资料(32份)（HarmonyOS/鸿蒙）》       [百度云下载链接](https://pan.baidu.com/s/13sWbBpRbDCG5jGmdVZjxZQ) 提取码:19x6
- 《把鸿蒙HarmonyOS liteos-a移植到STM32MP157开发手册》       [百度云下载链接](https://pan.baidu.com/s/13u-LLbGynCRAezf5Iq5U-Q) 提取码:bf9c
- 《安卓VS鸿蒙第三方件切换宝典 V1.0 》       [百度云下载链接](https://pan.baidu.com/s/1uG_ElVzV0DXH6dLVYX4X9g) 提取码:zod4


- 《经典资料：100个pdf开发文档》       [百度云下载链接](https://pan.baidu.com/s/1RELpgFDjZ2AxtkBQRUKT7A) 提取码:jc1v


#### 工具

- 《鸿蒙HarmonyOS移植树莓派搭建环境下载源码》       [百度云下载链接](https://pan.baidu.com/s/1ZxecoyMhURXsFB9Mo-Q8vA) 提取码:qino
- 《Linux下的Hi3861一站式鸿蒙 HarmonyOS开发烧录（附工具）》       [百度云下载链接](https://pan.baidu.com/s/1QrecjRhzlaAHH4blBFmZpw)  密码:43xj


#### 自定义组件

- 《全体注意！一大波鸿蒙 HarmonyOS三方库已经到来！》       [百度云下载链接](https://harmonyos.51cto.com/posts/4039)  密码:无
- 《快速体验鸿蒙 HarmonyOS Liteos-A：IMX6ULL的QEMU系统(Windows版本)》       [百度云下载链接](https://pan.baidu.com/s/1h5GntleWUsRdo8eSl4yDxw)  密码:kpbc


#### 框架项目

- 鸿蒙手表呼吸训练项目       [百度云下载链接](https://pan.baidu.com/s/17Sip9DMhUu7rmX2cpsfkoQ)  密码:k026
- 鸿蒙登录页面项目       [鸿蒙登录页面项目](https://gitee.com/codegrowth/haomony-develop/tree/master/LoginPage)
- 鸿蒙商城项目       [鸿蒙商城购物项目](https://gitee.com/codegrowth/haomony-develop/tree/master/ShoppingProject)                 
  商城项目拆解请查看知识星球 https://articles.zsxq.com/id_eatrh8qzxaub.html
- 鸿蒙头条首页项目       [鸿蒙头条首页项目](https://gitee.com/codegrowth/haomony-develop/tree/master/Todayeadlines)
- 鸿蒙头条项目       [鸿蒙头条首页项目](https://gitee.com/codegrowth/haomony-develop/tree/master/MyNewProject)


#### 绘制

- 鸿蒙手绘板 （含源代码）       [百度云下载链接](https://gitee.com/doufx/draw-component)  密码:无


#### 使用说明

1.  可以关注公众号 微信搜索：【沉默的闪客】 ，跟着文章一步一步学习鸿蒙开发。


#### 参与贡献

公众号：沉默的闪客，开源黑板报  

![输入图片说明](ArrayDev/qrcode_for_gh_ca16fc12725c_860.jpg)
![输入图片说明](ArrayDev/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20240407231558.jpg)

#### 转载

需要转载文章，请联系  公众号 号主 。