import { ConfirmOrderTitleBar } from '../ConfirmOrder/ConfirmOrderTitleBar'
import { EmptyComponent } from '../../common/EmptyComponent'
import { OrderListContent } from '../MyOrder/OrderListContent'
import { OrderOperationStatus, Order } from '../../Order/OrderModel'
import router from '@ohos.router';

@Entry
@Component
struct OrderDetailList {
  @State currentTabIndex: number = 0;

  build() {
    Flex({ direction: FlexDirection.Column }) {
      ConfirmOrderTitleBar({ title: '我的订单', onBack: () => {
        router.back();
      } })
      this.OrderTabs()
    }
    .backgroundColor('#f1f3f5')
  }

  @Builder OrderTabs() {
    Column() {
      Tabs({
        barPosition: BarPosition.Start,
        index: this.currentTabIndex
      }) {
        TabContent() {
          OrderListContent({
            status: OrderOperationStatus.ALLStatus,
          })
        }
        .tabBar(this.tabBar('全部', 0))

        TabContent() {
          OrderListContent({
            status: OrderOperationStatus.UN_PAY,
          })
        }
        .tabBar(this.tabBar('待付款', 1))

        TabContent() {
          Column() {
            EmptyComponent()
          }
          .width('100%')
          .height('100%')
        }
        .tabBar(this.tabBar('待发货', 2))

        TabContent() {
          OrderListContent({
            status: OrderOperationStatus.DELIVERED,
          })
        }
        .tabBar(this.tabBar('待收货', 3))

        TabContent() {
          OrderListContent({
            status: OrderOperationStatus.RECEIPT,
          })
        }
        .tabBar(this.tabBar('待评价', 4))
      }
      .barHeight(32)
      .barWidth('100%')
      .barMode(BarMode.Fixed)
      .margin({
        top: $r('app.float.vp_eight'), bottom: $r('app.float.vp_eight')
      })
      .onChange((index: number) => {
        this.currentTabIndex = index;
      })
    }
    .height('100%')
    .width('100%')
  }

  @Builder tabBar(text: string | Resource, index: number) {
    Flex({ justifyContent: FlexAlign.Center, alignItems: ItemAlign.Center }) {
      Text(text)
        .fontSize($r('app.float.middle_font_size'))
        .textAlign(TextAlign.Center)
        .fontColor(this.currentTabIndex === index ? '#e92f4f' : '#99000000')
        .fontWeight(this.currentTabIndex === index ? 500 : 400)
        .border({
          color: '#e92f4f',
          width: { bottom: this.currentTabIndex === index ? 2 : 0 }
        })
        .height(30)
    }
    .width('100%')
  }
}