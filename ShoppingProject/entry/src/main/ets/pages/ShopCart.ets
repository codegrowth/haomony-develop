import { shopCartData } from '../pages/Home/ShopData'
import { Product,SelectProducts } from '../pages/Home/ProductModel'
import { CountProduct } from '../pages/ShopCartView/CounProduct'
import router from '@ohos.router';

@Component
export struct ShopCart {
  @State shopCartData: Product[] = shopCartData;

  @State isSelectAll: boolean = false;
  @State sumPrice: number = 0;

  @State @Watch('selectProductChange') selectProducts: SelectProducts[] = [];

  aboutToAppear(){
    this.onListChange();
  }

  onListChange() {
    this.selectProducts = [];
    this.shopCartData.forEach((item: Product) => {
      let payload: SelectProducts = { selected: false, key: '' };
      payload.selected = !!item.selected;
      payload.key = item.id;
      this.selectProducts.push(payload);
    })
  }

  selectProductChange(index: number): boolean {
    if (this.selectProducts[index] !== undefined) {
      return this.selectProducts[index].selected;
    }
    return false;
  }

  build() {
    Flex({ direction: FlexDirection.Column,alignContent:FlexAlign.Start }) {
      Text($r('app.string.cart'))
        .fontSize(24)
        .height(56)
        .padding({ left: $r('app.float.vp_twenty_four') })
        .width('100%')
        .textAlign(TextAlign.Start)
      Scroll() {
        Column() {
          List({ space: 15 }) {
            ForEach(this.shopCartData, (item: Product, index?: number) => {
              ListItem() {
                if (index !== undefined) {
                  this.CartItem(item, index)
                }
              }
              .swipeAction({ end: this.ItemDelete(item) }) // 自定義滑動刪除效果
            }, (item: Product) => item.id)
          }
        }
        .height('100%')
      }
      .scrollable(ScrollDirection.Vertical)  // 滚动方向纵向
      .scrollBar(BarState.Off)
      .margin({
        left: $r('app.float.vp_twelve'),
        right: $r('app.float.vp_twelve')
      })
      .height('100%')
      /*底部結算*/
      if ((this.selectProducts).some(((item: SelectProducts) =>
      item.selected === true))) {
        this.Settle()
      }
    }
    .width('100%')
    .backgroundColor('#f1f3f5')
  }

  /*購物車列表*/
  @Builder
  CartItem(item: Product, index: number) {
    Flex({ direction: FlexDirection.Row, alignItems: ItemAlign.Center }) {
      Checkbox({
        name: `checkbox ${index}`,
        group: 'checkboxGroup'
      })
        .width($r('app.float.vp_twenty_four'))
        .height($r('app.float.vp_twenty_four'))
        .selectedColor('#ed6f21')
        .select(this.selectProductChange(index))
        .onClick(() => {
          console.log('=====' + index)
          let tempData: SelectProducts = {
            key: this.selectProducts[index].key,
            selected: !this.selectProducts[index].selected
          }
          this.selectProducts.splice(index, 1, tempData)
        })
      Image($rawfile(item.img[0]))
        .height(80)
        .width(80)
        .objectFit(ImageFit.Cover)
        .margin({ left: $r('app.float.vp_sixteen') })
      Flex({ direction: FlexDirection.Column, justifyContent: FlexAlign.SpaceAround }) {
        Text($r('app.string.commodity_piece_description', item.name, item.description))
          .fontSize($r('app.float.small_font_size'))
          .margin({ bottom: $r('app.float.vp_eight') })
          .textOverflow({ overflow: TextOverflow.Ellipsis })
          .maxLines(2)
          .width('100%')
        Text(item.specifications.length === 2 ?
          item.specifications[0].value + '，' + item.specifications[1].value:
            item.specifications.length === 3 ?
            item.specifications[0].value + '，' + item.specifications[1].value + '，' + item.specifications[2].value :
              item.specifications.length === 4 ?
              item.specifications[0].value + '，' + item.specifications[1].value + '，' + item.specifications[2].value + '，' + item.specifications[3].value : ''
        )
          .fontSize(12)
          .maxLines(1)
          .fontColor('#99000000')
          .textOverflow({ overflow: TextOverflow.Ellipsis })
          .width('100%')
        Flex({ justifyContent: FlexAlign.SpaceBetween }) {
          Text() {
            Span("￥")
              .fontSize(12)
              .fontColor('#e92f4f')
            Span(`${item.price}`)
              .fontSize($r('app.float.middle_font_size'))
              .fontColor('#e92f4f')
          }

          CountProduct({
            count: item.count,
            onNumberChange: (num: number) => {
              // this.onChangeCount(num, item);
            }
          })
        }
      }
      .margin({
        left: $r('app.float.vp_sixteen'),
        top: $r('app.float.vp_twelve'),
        bottom: $r('app.float.vp_twelve')
      })
      .width('100%')
    }
    .padding({
      left: $r('app.float.vp_twelve'),
      right: $r('app.float.vp_twelve')
    })
    .borderRadius($r('app.float.vp_sixteen'))
    .backgroundColor(Color.White)
    .width('100%')
    .height(112)
  }

  /*自定義滑動刪除效果*/
  @Builder
  ItemDelete(item: Product) {
    Flex({ direction: FlexDirection.Column, justifyContent: FlexAlign.Center,
      alignItems: ItemAlign.End }) {
      Column() {
        Image($r('app.media.ic_trash'))
          .width($r('app.float.vp_twenty_four'))
          .height($r('app.float.vp_twenty_four'))
          .margin({ bottom: 10 })
        Text("删除")
          .fontSize($r('app.float.small_font_size'))
          .fontColor(Color.White)
      }
      .padding({ right: $r('app.float.vp_fourteen') })
    }
    .onClick(() => {
    })
    .height(112)
    .width(80)
    .backgroundColor('#e92f4f')
    .borderRadius($r('app.float.vp_sixteen'))
    .margin({ left: -24 })
  }

  /*商品選中底部計算操作*/
  @Builder
  Settle() {
    Flex({ justifyContent: FlexAlign.SpaceBetween }) {
      Flex({ alignItems: ItemAlign.Center }) {
        Checkbox({ name: 'checkbox',
          group: 'checkboxGroup' })
          .selectedColor('#ed6f21')
          .select(this.isSelectAll)
          .onClick(() => {
          })
        Text('全选')
          .fontSize($r('app.float.small_font_size'))
          .fontColor(Color.Grey)
      }
      .height('100%')
      .width(100)

      Flex({ alignItems: ItemAlign.Center, justifyContent: FlexAlign.End }) {
        Text('合计:')
          .fontSize(14)
          .fontWeight(FontWeight.Bold)
        Text() {
          Span("￥")
            .fontSize(14)
            .fontColor("#e92f4f")
          Span(`${this.sumPrice}`)
            .fontSize($r('app.float.middle_font_size'))
            .fontColor("#e92f4f")
        }
        .margin({
          right: $r('app.float.vp_eight'),
          left: $r('app.float.vp_eight')
        })

        Button("结算",
          { type: ButtonType.Capsule, stateEffect: true })
          .backgroundColor("#e92f4f")
          .fontSize(14)
          .height(40)
          .width(100)
          .onClick(() => {
            router.pushUrl({
              url:'pages/Order/ConfirmOrder/ConfirmOrderPage'
            })
          })
      }
      .height('100%')
    }
    .padding({
      right: $r('app.float.vp_twelve'),
      left: $r('app.float.vp_twelve')
    })
    .backgroundColor(Color.White)
    .width('100%')
    .height(56)
  }
}