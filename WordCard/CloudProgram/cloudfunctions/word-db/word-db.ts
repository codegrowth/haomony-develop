import { CloudDBZoneWrapper } from '../word-db/clouddb/CloudDBZoneWrapper';
import * as Utils from '../word-db/utils/Utils';

export const myHandler = async function (event, context, callback, logger) {
  const credential = Utils.getCredential(context, logger);

  try {
    const cloudDBZoneWrapper = new CloudDBZoneWrapper(credential, logger);
    const result = await cloudDBZoneWrapper.queryWordKeyData();
    if (result) {
      callback({
        ret: { code: 0, desc: "SUCCESS" },
        result: [...result],
        // result: ['111','222'],
      });
    }
  } catch (err) {
    logger.error("func error:" + err.message + " stack:" + err.stack);
    callback({
      ret: { code: -1, desc: "ERROR" },
    });
  }
};
